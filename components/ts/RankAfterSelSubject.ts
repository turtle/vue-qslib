import BaseQuestionSubject from './TQuestionSubject';
import BaseOption from './BaseOption';

// 排序选项
export class RankAfterSelOption extends BaseOption {
    // 排序的位置
    public OrderIndex: number = 0;
    public HasChecked: boolean = false;
     // 选择子项目
    public SubItems: RankAfterSelOption[] = [];
}
export default class RankAfterSubject extends BaseQuestionSubject<RankAfterSelOption> {
    public static option: RankAfterSelOption;
    // 选中的值
    public CheckedValues: string[] = [];
    // 最多选择
    public MaxSel: number = 0;


}
